@echo off
color 3F
title  加入Chia技术交流群:178516844
REM 设置参数
set Processes=3
set lodingTime=50
set PlotSize=32
set BufferSize=3390
set buckets=128
set threads=2
set temp=D:\
set Result=D:\
REM overStart参数检查chia.exe进程数 等于该值时启动p图任务
set overStart=0
set _task=chia.exe
REM %userprofile%可以改成绝对路径，这里是1.1.2版本路径，更新后注意路径变化
set _svr=%userprofile%\AppData\Local\chia-blockchain\app-1.1.2\resources\app.asar.unpacked\daemon\chia.exe
echo %_svr%
:checkService
for /f "tokens=1" %%n in ('wmic path win32_process get name^,commandline ^| find "chia.exe plots" ^|find "find" /v /c') do (
     echo 当前p盘进程数量为:%%n
     if %%n leq %overStart% ( goto restartService) else goto checkMessage
)


:restartService
set mydate=%date:~0,10%
set mytime=%time:~0,8%
echo %mydate% %mytime%
echo ********程序开始启动********
set st=0

echo 并发数量:%Processes% 延迟时间:%lodingTime%秒 内存大小:%BufferSize%  桶数量%buckets% 线程数量:%threads% 缓存目录:%temp% 结果目录:%Result%

:start
set /a st+=1
set mydate=%date:~0,10%
set mytime=%time:~0,8%
if %st% leq %Processes% (


echo 启动 %st%/%Processes% 个进程 %mytime% ...
start "Chia当前p盘进程%st%/%Processes%  启动时间:%mydate% %mytime% 欢迎使用本程序!"  cmd /C %_svr% plots create -k %PlotSize% -b %BufferSize% -u %buckets% -r %threads% -t %temp% -d %Result%
if %st%  lss %Processes% ( 
echo 启动完成 等待%lodingTime%秒启动下一个
set /p=.<nul
for /L %%i in (1 1 %lodingTime%) do set /p a=.<nul & ping.exe /n 2 127.0.0.1>nul
echo .
)
) else (
echo .
echo ********程序启动完成********
echo ****************************
echo .
echo .
echo .
goto checkService)
goto start
:checkMessage

echo %date:~0,10% %time:~0,8% 程序运行正常,30秒后继续检查..
REM set /p=.<nul 不换行在屏幕输出....
set /p=.<nul
for /L %%i in (1 1 30) do set /p a=.<nul & ping.exe /n 2 127.0.0.1>nul
echo .
goto checkService